#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
 * Display the board.
 */
void print_board(char b[][8])
{
    for (int row = 0; row < 8; row++)
    {
        for (int col = 0; col < 8; col++)
        {
            printf("%c ", b[row][col]);
        }
        printf("\n");
    }
}

/*
 * Initialize the board to be all underscores.
 */
void init_board(char b[][8])
{
    for (int row = 0; row < 8; row++)
    {
        for (int col = 0; col < 8; col++)
        {
            b[row][col] = '_';
        }
    }
}

/*
 * Get a move from the user.
 *
 *   2   3
 * q       e
 *     K
 * a       d
 *   z   x
 *
 */
char get_move()
{
    char m;
    int valid = 0;
    do
    {
        printf("What's your move? ");
        scanf(" %c", &m);
        if (m != '2' && m != '3' && m != 'e' && m != 'd' &&
            m != 'x' && m != 'z' && m != 'a' && m != 'q')
        {
            printf("Moves are 2, 3, e, d, x, z, a, q\n");
        }
        else
        {
            valid = 1;
        }
    } while (! valid);
    
    return m;
}

/*
 * The function is passed the knight's x and y location,
 * plus the user's move (from get_move()).
 * The move will be one of 2, 3, e, d, x, z, a, q.
 * The function should change the x and y location based
 * on the direction the user selected.
 * The function returns 1 (true) if the move was valid.
 * Returns 0 if the move would cause the knight to move
 * off the board. If the move is invalid, the knight's
 * position does not change.
 *
 * Write your move_knight function here.
 */

int move_knight( int *x, int *y, char m)
{
   //printf("%d, %d\n", *x, *y);
   if(m == '2')
   {
     if((*x)-1 > -1 && (*y) - 2 > -1) 
     {
     *x = (*x)-1;
     *y = (*y)-2;
     return 1;
     }
   }
   else if(m == '3')
   {
     if((*x)+1 < 8  && (*y)-2 > -1) 
      { 
     *x = (*x)+1;
     *y = (*y)-2;
      return 1;
     }
   }
   else if(m == 'e')
   {
     if((*x)+2 < 8 && (*y)-1 > -1) 
     { 
     *x = (*x)+2;
     *y = (*y)-1;
     return 1;
     }
   }
   else if(m == 'd')
   {
     if((*x)+2 < 8 && (*y)+1 < 8) 
     {
     *x = (*x)+2;
     *y = (*y)+1;
     return 1;
     }
   }
   else if(m == 'q')
   {
     if((*x)-2 > -1 && (*y)-1 < 8) 
     {
     *x = (*x)-2;
     *y = (*y)-1;
        return 1;
     }
   }
   else if(m == 'a')
   {
     if((*x)-2 > -1 && (*y)+1 < 8) 
     {
     *x = (*x)-2;
     *y = (*y)+1;
        return 1;
     }
   }
   else if(m == 'z')
   {
     if((*x)-1 > -1 && (*y)+2 < 8) 
     {
     *x = (*x)-1;
     *y = (*y)+2;
        return 1;
     }
   }
   else if(m == 'x')
   {
     if((*x)+1 < 8 && (*y)+2 < 8) 
     {
     *x = (*x)+1;
     *y = (*y)+2;
     return 1;
     }
   }
   return 0;
}

int main()
{
    char board[8][8];
    int kx = 1, ky = 7;   // Starting location for knight
    
    srand(time(NULL));    // Seed the random number generator
    
    int tx, ty;           // Target location
    
    do
    {
        tx = rand() % 8;  // Pick random target
        ty = rand() % 8;
    } while (tx == kx && ty == ky);
    
    init_board(board);
    board[ty][tx] = 'G';    // Place goal and knight
    board[ky][kx] = 'K';
    
    print_board(board);
    
    int moves = 0;
    
    while (tx != kx || ty != ky)
    {
        char move = get_move();
        board[ky][kx] = '_';
        int ret = move_knight(&kx, &ky, move);
        board[ky][kx] = 'K';
        if (!ret)
        {
            printf("Invalid move.\n");
        }
        else
        {
            print_board(board);
            moves++;
        }
    }
    printf("You won in %d moves!\n", moves);
}