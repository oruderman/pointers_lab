
#include <stdio.h>
#include <ctype.h>
#include <string.h>

int strcaps(char *str);

int main()
{
  char str[100];
    printf("Enter a string: ");
    fgets(str, 100, stdin);
    strcaps(str);
    printf("Converted string: %s\n", str);
    return 0;
}

int strcaps(char *str) 
{
    int i;
    for (i = 0; i < strlen(str); i++)
        str[i] = toupper(str[i]);
    return i;
}
